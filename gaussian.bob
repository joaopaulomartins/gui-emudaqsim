<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>Gaussian Simulation - Overview</name>
  <macros>
    <P>LAB-INFN:Ctrl-AMC-001:Gauss</P>
    <P2>LAB-INFN:Ctrl-AMC-001:</P2>
    <R></R>
  </macros>
  <width>1380</width>
  <height>1060</height>
  <widget type="rectangle" version="2.0.0">
    <name>Titlebar</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>1380</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <class>TITLE</class>
    <text>Gaussian Test</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>280</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="xyplot" version="2.0.0">
    <name>X/Y Plot</name>
    <x>20</x>
    <y>120</y>
    <width>1340</width>
    <height>520</height>
    <x_axis>
      <title>X</title>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
      <minimum>0.0</minimum>
      <maximum>100.0</maximum>
      <show_grid>true</show_grid>
      <title_font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </title_font>
      <scale_font>
        <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </scale_font>
      <visible>true</visible>
    </x_axis>
    <y_axes>
      <y_axis>
        <title>Y</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <show_grid>true</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </y_axis>
    </y_axes>
    <traces>
      <trace>
        <name>$(traces[0].y_pv)</name>
        <x_pv></x_pv>
        <y_pv>$(P)$(R)Gaussian</y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="0" green="0" blue="255">
          </color>
        </color>
        <line_width>1</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
    </traces>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>Peak</text>
    <x>65</x>
    <y>70</y>
    <width>60</width>
    <height>30</height>
  </widget>
  <widget type="textentry" version="3.0.0">
    <name>Text Entry</name>
    <pv_name>$(P)$(R)Peak</pv_name>
    <x>125</x>
    <y>70</y>
    <width>130</width>
    <height>30</height>
    <format>3</format>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_1</name>
    <text>Center</text>
    <x>315</x>
    <y>70</y>
    <width>60</width>
    <height>30</height>
  </widget>
  <widget type="textentry" version="3.0.0">
    <name>Text Entry_1</name>
    <pv_name>$(P)$(R)Center</pv_name>
    <x>375</x>
    <y>70</y>
    <width>130</width>
    <height>30</height>
    <format>1</format>
    <precision>3</precision>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_2</name>
    <text>StdDev</text>
    <x>565</x>
    <y>70</y>
    <width>60</width>
    <height>30</height>
  </widget>
  <widget type="textentry" version="3.0.0">
    <name>Text Entry_2</name>
    <pv_name>$(P)$(R)StdDev</pv_name>
    <x>625</x>
    <y>70</y>
    <width>130</width>
    <height>30</height>
    <format>3</format>
    <precision>6</precision>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Boolean Button</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>WritePV</description>
      </action>
    </actions>
    <pv_name>$(P)$(R)StartCalc</pv_name>
    <text>Calculate</text>
    <x>815</x>
    <y>70</y>
    <width>140</width>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group</name>
    <x>20</x>
    <y>700</y>
    <width>840</width>
    <height>350</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="tank" version="2.0.0">
      <name>Tank</name>
      <pv_name>$(P2)CH1-STAT2-MeanValue_RBV</pv_name>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_1</name>
      <pv_name>$(P2)CH2-STAT2-MeanValue_RBV</pv_name>
      <x>35</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_2</name>
      <pv_name>$(P2)CH3-STAT2-MeanValue_RBV</pv_name>
      <x>70</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_3</name>
      <pv_name>$(P2)CH4-STAT2-MeanValue_RBV</pv_name>
      <x>105</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_4</name>
      <pv_name>$(P2)CH5-STAT2-MeanValue_RBV</pv_name>
      <x>140</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_5</name>
      <pv_name>$(P2)CH6-STAT2-MeanValue_RBV</pv_name>
      <x>175</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_6</name>
      <pv_name>$(P2)CH7-STAT2-MeanValue_RBV</pv_name>
      <x>210</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_7</name>
      <pv_name>$(P2)CH8-STAT2-MeanValue_RBV</pv_name>
      <x>245</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_8</name>
      <pv_name>$(P2)CH9-STAT2-MeanValue_RBV</pv_name>
      <x>280</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_9</name>
      <pv_name>$(P2)CH10-STAT2-MeanValue_RBV</pv_name>
      <x>315</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_10</name>
      <pv_name>$(P2)CH11-STAT2-MeanValue_RBV</pv_name>
      <x>350</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_11</name>
      <pv_name>$(P2)CH12-STAT2-MeanValue_RBV</pv_name>
      <x>385</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_12</name>
      <pv_name>$(P2)CH13-STAT2-MeanValue_RBV</pv_name>
      <x>420</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_13</name>
      <pv_name>$(P2)CH14-STAT2-MeanValue_RBV</pv_name>
      <x>455</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_14</name>
      <pv_name>$(P2)CH15-STAT2-MeanValue_RBV</pv_name>
      <x>490</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_15</name>
      <pv_name>$(P2)CH16-STAT2-MeanValue_RBV</pv_name>
      <x>525</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_16</name>
      <pv_name>$(P2)CH17-STAT2-MeanValue_RBV</pv_name>
      <x>560</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_17</name>
      <pv_name>$(P2)CH18-STAT2-MeanValue_RBV</pv_name>
      <x>595</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_18</name>
      <pv_name>$(P2)CH19-STAT2-MeanValue_RBV</pv_name>
      <x>630</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_19</name>
      <pv_name>$(P2)CH20-STAT2-MeanValue_RBV</pv_name>
      <x>665</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_20</name>
      <pv_name>$(P2)CH21-STAT2-MeanValue_RBV</pv_name>
      <x>700</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_21</name>
      <pv_name>$(P2)CH22-STAT2-MeanValue_RBV</pv_name>
      <x>735</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_22</name>
      <pv_name>$(P2)CH23-STAT2-MeanValue_RBV</pv_name>
      <x>770</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>Tank_23</name>
      <pv_name>$(P2)CH24-STAT2-MeanValue_RBV</pv_name>
      <x>805</x>
      <width>35</width>
      <height>350</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <limits_from_pv>false</limits_from_pv>
      <maximum>2.5E-4</maximum>
    </widget>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_3</name>
    <text>Wires Measurement (ROI values)</text>
    <x>20</x>
    <y>670</y>
    <width>360</width>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group</name>
    <macros>
      <AXIS>Horizontal Slit</AXIS>
      <M>Axis1</M>
      <P>$(P2)</P>
      <R>Axis1-</R>
    </macros>
    <x>930</x>
    <y>700</y>
    <width>410</width>
    <height>80</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="textentry" version="3.0.0">
      <name>Text Entry_3</name>
      <pv_name>$(P)$(M).VAL</pv_name>
      <x>10</x>
      <y>40</y>
      <width>130</width>
      <height>30</height>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update</name>
      <pv_name>$(P)$(M).RBV</pv_name>
      <x>160</x>
      <y>40</y>
      <width>130</width>
      <height>30</height>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button</name>
      <actions>
        <action type="open_display">
          <file>ecmcgeneralopi/ecmcOneAxisEngineering.bob</file>
          <target>tab</target>
          <description>Open Display</description>
        </action>
      </actions>
      <text>Open OPI</text>
      <x>310</x>
      <y>40</y>
      <width>90</width>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_4</name>
      <text>$(AXIS)</text>
      <x>10</x>
      <y>10</y>
      <width>390</width>
      <height>30</height>
      <font>
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group_1</name>
    <macros>
      <AXIS>Horizontal Grid</AXIS>
      <M>Axis2</M>
      <P>$(P2)</P>
      <R>Axis2-</R>
    </macros>
    <x>930</x>
    <y>790</y>
    <width>410</width>
    <height>80</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="textentry" version="3.0.0">
      <name>Text Entry_4</name>
      <pv_name>$(P)$(M).VAL</pv_name>
      <x>10</x>
      <y>40</y>
      <width>130</width>
      <height>30</height>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_1</name>
      <pv_name>$(P)$(M).RBV</pv_name>
      <x>160</x>
      <y>40</y>
      <width>130</width>
      <height>30</height>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_1</name>
      <actions>
        <action type="open_display">
          <file>ecmcgeneralopi/ecmcOneAxisEngineering.bob</file>
          <target>tab</target>
          <description>Open Display</description>
        </action>
      </actions>
      <text>Open OPI</text>
      <x>310</x>
      <y>40</y>
      <width>90</width>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_5</name>
      <text>$(AXIS)</text>
      <x>10</x>
      <y>10</y>
      <width>390</width>
      <height>30</height>
      <font>
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group_2</name>
    <macros>
      <AXIS>Vertical Slit</AXIS>
      <M>Axis3</M>
      <P>$(P2)</P>
      <R>Axis3-</R>
    </macros>
    <x>930</x>
    <y>880</y>
    <width>410</width>
    <height>80</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="textentry" version="3.0.0">
      <name>Text Entry_5</name>
      <pv_name>$(P)$(M).VAL</pv_name>
      <x>10</x>
      <y>40</y>
      <width>130</width>
      <height>30</height>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_2</name>
      <pv_name>$(P)$(M).RBV</pv_name>
      <x>160</x>
      <y>40</y>
      <width>130</width>
      <height>30</height>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_2</name>
      <actions>
        <action type="open_display">
          <file>ecmcgeneralopi/ecmcOneAxisEngineering.bob</file>
          <target>tab</target>
          <description>Open Display</description>
        </action>
      </actions>
      <text>Open OPI</text>
      <x>310</x>
      <y>40</y>
      <width>90</width>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_6</name>
      <text>$(AXIS)</text>
      <x>10</x>
      <y>10</y>
      <width>390</width>
      <height>30</height>
      <font>
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group_3</name>
    <macros>
      <AXIS>Vertical Grid</AXIS>
      <M>Axis4</M>
      <P>$(P2)</P>
      <R>Axis4-</R>
    </macros>
    <x>930</x>
    <y>970</y>
    <width>410</width>
    <height>80</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="textentry" version="3.0.0">
      <name>Text Entry_6</name>
      <pv_name>$(P)$(M).VAL</pv_name>
      <x>10</x>
      <y>40</y>
      <width>130</width>
      <height>30</height>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_3</name>
      <pv_name>$(P)$(M).RBV</pv_name>
      <x>160</x>
      <y>40</y>
      <width>130</width>
      <height>30</height>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_3</name>
      <actions>
        <action type="open_display">
          <file>ecmcgeneralopi/ecmcOneAxisEngineering.bob</file>
          <target>tab</target>
          <description>Open Display</description>
        </action>
      </actions>
      <text>Open OPI</text>
      <x>310</x>
      <y>40</y>
      <width>90</width>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_7</name>
      <text>$(AXIS)</text>
      <x>10</x>
      <y>10</y>
      <width>390</width>
      <height>30</height>
      <font>
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
  <widget type="choice" version="2.0.0">
    <name>Choice Button</name>
    <pv_name>$(P)$(R)TrackAxisSel</pv_name>
    <x>980</x>
    <y>660</y>
    <width>310</width>
    <height>30</height>
    <items>
      <item>Item 1</item>
      <item>Item 2</item>
    </items>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Menu Button_3</name>
    <actions>
      <action type="open_display">
        <file>main/system_top.bob</file>
        <macros>
          <Name>EMU DAQ Simulator</Name>
          <P>LAB-INFN:</P>
          <P1>LAB-INFN:</P1>
          <R>Ctrl-AMC-001:</R>
          <R1>Ctrl-AMC-001:</R1>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>Open DAQ Simulator</text>
    <x>1170</x>
    <y>10</y>
    <width>190</width>
    <font>
      <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <tooltip>$(actions)</tooltip>
  </widget>
</display>
